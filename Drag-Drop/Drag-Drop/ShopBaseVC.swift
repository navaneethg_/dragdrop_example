//
//  ShopBaseVC.swift
//  Drag-Drop
//
//  Created by navaneeth on 12/12/18.
//  Copyright © 2018 com.sprint. All rights reserved.
//

import UIKit

class ShopBaseVC : UIViewController {

    var deviceViews: [DeviceView] = []
    var deviceEntities: [DeviceEntity] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        deviceViews = getDeviceViews()
        deviceEntities = getDevices()
    }

    private func getDevices() -> [DeviceEntity] {
        let gold = UIColor(red: 252.0 / 255.0, green: 194.0 / 255.0, blue: 0, alpha: 1.0)
        let silver = UIColor(red: 192/255, green: 192/255, blue: 192/255, alpha: 1.0)
        return [DeviceEntity(id: 1, price: "$610", name: "iphone 6", color: gold, size: "128Gb"),
                DeviceEntity(id: 1, price: "$690", name: "iphone 6s", color: silver, size: "256Gb"),
                DeviceEntity(id: 1, price: "$710", name: "iphone 7", color: gold, size: "128Gb"),
                DeviceEntity(id: 1, price: "$790", name: "iphone 7+", color: silver, size: "64Gb"),
                DeviceEntity(id: 1, price: "$810", name: "iphone 8", color: gold, size: "256Gb"),
                DeviceEntity(id: 1, price: "$890", name: "iphone 8+", color: gold, size: "64Gb"),
                DeviceEntity(id: 1, price: "$910", name: "iphone X", color: silver, size: "128Gb"),
                DeviceEntity(id: 1, price: "$990", name: "iphone Xs", color: silver, size: "256Gb"),
                DeviceEntity(id: 1, price: "$867", name: "iphone Xr", color: gold, size: "128Gb")]
    }

    private func getDeviceViews() -> [DeviceView] {
        var views: [DeviceView] = []
        for device in getDevices() {
            if let view = Bundle.main.loadNibNamed("DeviceView", owner: nil, options: nil)?.last as? DeviceView {
                view.name.text = device.name
                view.price.text = device.price
                view.memory1.titleLabel?.text = device.size
                views.append(view)
            }
        }
        return views
    }
}
