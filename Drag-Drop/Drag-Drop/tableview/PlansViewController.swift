//
//  PlansViewController.swift
//  Drag-Drop
//
//  Created by navaneeth on 2/5/19.
//  Copyright © 2019 com.sprint. All rights reserved.
//

import UIKit
class PlansViewController: UITableViewController {

    let plans = [Plan(name: "Unlimited Basic", cost: "$60.0/m", hotspot: "10GB", extras: false), Plan(name: "Unlimited Plus", cost: "$70.0/m", hotspot: "50GB", extras: true), Plan(name: "Unlimited Premium", cost: "$90.0/m", hotspot: "70GB", extras: true),Plan(name: "Unlimited ", cost: "$40.0/m", hotspot: "2GB", extras: false)]

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Plans"
        tableView.register(UINib(nibName: "PlansCell", bundle: nil), forCellReuseIdentifier: "planscell")
        tableView.dataSource = self
        tableView.dragDelegate = self
        tableView.dragInteractionEnabled = true
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return plans.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let plan = plans[indexPath.row]
        guard let  cell = tableView.dequeueReusableCell(withIdentifier: "planscell", for: indexPath) as? PlansCell else {
            return UITableViewCell()
        }
        cell.planName.text = plan.planName
        cell.cost.text = plan.cost
        cell.extras.isHidden = plan.extras
        cell.hotspot.text = plan.hotspot
        return cell
    }
}

extension PlansViewController: UITableViewDragDelegate {

    func tableView(_ tableView: UITableView,
                   itemsForBeginning session: UIDragSession,
                   at indexPath: IndexPath) -> [UIDragItem] {

        let plan = plans[indexPath.row]
        let dragItem = UIDragItem(itemProvider: NSItemProvider(object: plan.planName as NSString))
        dragItem.localObject = plan
        return [dragItem]
    }

    func tableView(_ tableView: UITableView,
                   itemsForAddingTo session: UIDragSession,
                   at indexPath: IndexPath,
                   point: CGPoint) -> [UIDragItem] {

        let plan = plans[indexPath.row]
        let dragItem = UIDragItem(itemProvider: NSItemProvider(object: plan.planName as NSString))
        dragItem.localObject = plan
        return [dragItem]
    }
}
