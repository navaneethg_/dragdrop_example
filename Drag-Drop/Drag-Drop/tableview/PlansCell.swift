//
//  PlansCell.swift
//  Drag-Drop
//
//  Created by navaneeth on 2/5/19.
//  Copyright © 2019 com.sprint. All rights reserved.
//

import UIKit

class PlansCell: UITableViewCell {
    @IBOutlet weak var planName: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var extras: UILabel!
    @IBOutlet weak var hotspot: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.lightGray
        self.layer.cornerRadius = 15
        self.layer.masksToBounds = true
         self.contentView.backgroundColor = UIColor.lightGray
        self.contentView.layer.cornerRadius = 15
        self.contentView.layer.masksToBounds = true
    }
}
