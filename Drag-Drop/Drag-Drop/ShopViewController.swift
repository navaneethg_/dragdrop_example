//
//  ShopViewController.swift
//  Drag-Drop
//
//  Created by navaneeth on 12/12/18.
//  Copyright © 2018 com.sprint. All rights reserved.
//

import UIKit

class ShopViewController: UITabBarController {
    var isDragActive = false
    var devicesInCart : [Int] = []
    var plansInCart : [Plan] = []

    @IBOutlet weak var shopTabBar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        setAppreances()
    }

    func setAppreances() {
        delegate = self
        setDropInteractions()
        navigationItem.title = "Shop"
        if let tabbarItems = shopTabBar.items {
            for item in tabbarItems  {
                switch item.tag {
                case 1:
                    item.image = getScaledImage(with: UIImage(named: "shop_devices"))?.withRenderingMode(.alwaysOriginal)
                case 2:
                    item.image  = getScaledImage(with: UIImage(named: "compare"))?.withRenderingMode(.alwaysOriginal)
                case 3:
                    item.image = getScaledImage(with: UIImage(named: "cart"))?.withRenderingMode(.alwaysOriginal)
                default:
                    break
                }
            }
        }
    }

    func getScaledImage(with image: UIImage?, scaledTo newSize: CGSize = CGSize(width: 30, height: 30)) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(newSize, _: false, _: 0.0)
        image?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }

    func setDropInteractions(){
        //        let x =  UISpringLoadedInteraction(interactionBehavior: nil, interactionEffect: nil, activationHandler: {_, _ in
        //            print("----do nothing")
        //        })
        shopTabBar.isSpringLoaded = true
        shopTabBar.addInteraction(UIDropInteraction(delegate: self))

    }
}

extension ShopViewController: UIDropInteractionDelegate {

    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return true
    }

    func dropInteraction(_ interaction: UIDropInteraction, sessionDidEnter session: UIDropSession) {
        isDragActive = true
    }

    func dropInteraction(_ interaction: UIDropInteraction, sessionDidEnd session: UIDropSession) {
        isDragActive = false
    }

    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        let dropLocation = session.location(in: shopTabBar)
        let cartTabFrame : CGRect = shopTabBar.subviews[3].frame

        if cartTabFrame.contains(dropLocation) {
            return UIDropProposal(operation: .copy)

        }
        return UIDropProposal(operation: .cancel)
    }

    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        for item in session.items {
            if let index = item.localObject as? Int {
                devicesInCart.append(index)
                setCartBadge()
            }
            if let plan = item.localObject as? Plan {
                plansInCart.append(plan)
            }
        }
    }

    func setCartBadge() {
        if let cartVC =  (self.viewControllers?.filter({$0 is CartViewController}).last as? CartViewController) {
            cartVC.setBadge(value: devicesInCart.count)
        }
    }
}

extension ShopViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return isDragActive && viewController.isKind(of: CartViewController.self) ? false : true
    }
}


