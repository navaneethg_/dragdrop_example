//
//  ViewController.swift
//  Drag-Drop
//
//  Created by navaneeth on 12/11/18.
//  Copyright © 2018 com.sprint. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var compareBtn: UIButton!
    @IBOutlet weak var sprintloadBtn: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.lightGray
        setBtnAppreances()
    }

    func setBtnAppreances() {
        navigationItem.title = "Home"
    }
}

