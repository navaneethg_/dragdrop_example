//
//  CartView.swift
//  Drag-Drop
//
//  Created by navaneeth on 12/17/18.
//  Copyright © 2018 com.sprint. All rights reserved.
//

import UIKit

class CartView : UIView {

    @IBOutlet weak var deviceImageview: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var color: UILabel!
    @IBOutlet weak var size: UILabel!
    @IBOutlet weak var price: UILabel!

}
