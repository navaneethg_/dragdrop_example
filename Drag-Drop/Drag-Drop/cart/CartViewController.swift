//
//  CartViewController.swift
//  Drag-Drop
//
//  Created by navaneeth on 12/12/18.
//  Copyright © 2018 com.sprint. All rights reserved.
//

import UIKit

class CartViewController: ShopBaseVC {
    @IBOutlet weak var cartTabItem: UITabBarItem!
    @IBOutlet weak var cartStack: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setAppreances()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cartStack.arrangedSubviews.forEach { $0.removeFromSuperview() }
        addCartViews()
        addPlanViews()
    }
    
    func setAppreances() {
        
    }
    
    func addCartViews() {
        if let tabVc = self.tabBarController as? ShopViewController {
            for device in tabVc.devicesInCart {
                if let view = Bundle.main.loadNibNamed("CartView", owner: nil, options: nil)?.last as? CartView {
                    view.deviceImageview.image = deviceEntities[device].image
                    view.name.text = deviceEntities[device].name
                    view.color.backgroundColor = deviceEntities[device].color
                    view.size.text = deviceEntities[device].size
                    view.price.text = deviceEntities[device].price
                    cartStack.addArrangedSubview(view)
                }
            }
        }
    }
    
    func addPlanViews() {
        if let tabVc = self.tabBarController as? ShopViewController {
            if let plan = tabVc.plansInCart.last,
                let cell = Bundle.main.loadNibNamed("PlansCell", owner: nil, options: nil)?.last as? PlansCell  {
                cell.planName.text = plan.planName
                cell.cost.text = plan.cost
                cell.extras.isHidden = plan.extras
                cell.hotspot.text = plan.hotspot
                cartStack.addArrangedSubview(cell.contentView)
            }
            
        }
    }
    
    func setBadge(value: Int = 0) {
        cartTabItem.badgeValue = String(value)
        cartTabItem.badgeColor = UIColor.blue
    }
    
    
}
