//
//  CompareViewController.swift
//  Drag-Drop
//
//  Created by navaneeth on 12/12/18.
//  Copyright © 2018 com.sprint. All rights reserved.
//

import UIKit

class CompareViewController: ShopBaseVC {

    @IBOutlet weak var dropHere: UILabel!
    @IBOutlet weak var plansstack: UIStackView!
    @IBOutlet weak var compareTabItem: UITabBarItem!

    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var price1: UILabel!
    @IBOutlet weak var price2: UILabel!
    @IBOutlet weak var color1: UILabel!
    @IBOutlet weak var color2: UILabel!
    @IBOutlet weak var memory1: UILabel!
    @IBOutlet weak var memory2: UILabel!


    @IBOutlet weak var planname1: UILabel!
    @IBOutlet weak var planname2: UILabel!

    @IBOutlet weak var planprice1: UILabel!
    @IBOutlet weak var planprice2: UILabel!

    @IBOutlet weak var data1: UILabel!
    @IBOutlet weak var data2: UILabel!

    @IBOutlet weak var extra1: UILabel!
    @IBOutlet weak var extra2: UILabel!

    @IBOutlet weak var hotspot1: UILabel!
    @IBOutlet weak var hotspot2: UILabel!
    

    var device1Index : Int = -1
    var device2Index : Int = -1

    override func viewDidLoad() {
        super.viewDidLoad()
        setAppreances()
    }

    func setAppreances() {
        plansstack.alpha = 0.0
        dropHere.layer.cornerRadius = 15
        dropHere.layer.masksToBounds = true
        image1.isUserInteractionEnabled = true
        image2.isUserInteractionEnabled = true
        addDragInteractions()
        addDropInteractions()
    }

    func addDragInteractions(){

        let draginteration = UIDragInteraction(delegate: self)
        draginteration.isEnabled = true
        view.addInteraction(draginteration)
        //        image2.addInteraction(draginteration)
    }

    func addDropInteractions(){
        image1.addInteraction(UIDropInteraction(delegate: self))
        image2.addInteraction(UIDropInteraction(delegate: self))
        dropHere.addInteraction(UIDropInteraction(delegate: self))
    }
}

extension CompareViewController: UIDropInteractionDelegate {

    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.items.count >= 0
    }

    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        let dropLocation = session.location(in: view)

        if image2.frame.contains(dropLocation) || image1.frame.contains(dropLocation) {
            indiacateDropArea(inside : true)
            return UIDropProposal(operation: .copy)
        } else if let dropHereLbl = view?.hitTest(dropLocation, with: nil) as? UILabel, dropHereLbl == dropHere,
            session.canLoadObjects(ofClass: NSString.self) {
            return UIDropProposal(operation: .copy)
        } else {
            indiacateDropArea(inside : false)
            return UIDropProposal(operation: .cancel)
        }
    }

    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        let dropLocation = session.location(in: view)
        var flag = 0
        for item in session.items {
            flag += 1
            if let index = item.localObject as? Int {
                if session.items.count > 1 {
                    _ = flag == 1 ?
                        updateDevice1(index: index) :
                        updateDevice2(index: index)
                } else {
                    _ = image1.frame.contains(dropLocation) ?
                        updateDevice1(index: index) :
                        updateDevice2(index: index)
                }
            }

            if let dropHereLbl = view?.hitTest(dropLocation, with: nil) as? UILabel, dropHereLbl == dropHere,
                let plan = item.localObject as? Plan {
                if session.items.count > 1 {
                    _ = flag == 1 ?
                        performPlanDrop(planA: nil, planB: plan) :
                        performPlanDrop(planA: plan, planB: nil)
                } else {
                    performPlanDrop(planA: nil, planB: plan)
                }
            }
        }
        indiacateDropArea(inside: false)
    }

    func performPlanDrop(planA: Plan?, planB: Plan?) {
        plansstack.alpha = 1.0
        if let plan1 =  planA {
            planname1.text = plan1.planName
            planprice1.text = plan1.cost
            data1.text = "Unlimited"
            if plan1.extras {
                extra1.text = "Hulu/Tidal"
            } else {
                extra1.text = "-"
            }
            hotspot1.text = plan1.hotspot
        } else if let plan2 = planB {
            planname2.text = plan2.planName
            planprice2.text = plan2.cost
            data2.text = "Unlimited"
            if plan2.extras {
                extra2.text = "Hulu/Tidal"
            } else {
                extra2.text = "-"
            }
            hotspot2.text = plan2.hotspot
        }
    }

    func updateDevice1(index : Int){
        image1.image =  deviceViews[index].deviceImageview.image
        name1.text = deviceEntities[index].name
        price1.text = deviceEntities[index].price
        color1.backgroundColor = deviceEntities[index].color
        memory1.text =  deviceEntities[index].size
        device1Index = index
    }

    func updateDevice2(index : Int){
        image2.image =  deviceViews[index].deviceImageview.image
        name2.text = deviceEntities[index].name
        price2.text = deviceEntities[index].price
        color2.backgroundColor = deviceEntities[index].color
        memory2.text =  deviceEntities[index].size
        device2Index = index
    }

    func dropInteraction(_ interaction: UIDropInteraction, sessionDidEnd session: UIDropSession) {
        indiacateDropArea(inside: false)
    }

    func indiacateDropArea(inside : Bool){
        if inside {
            image1.backgroundColor = UIColor.gray
            image2.backgroundColor = UIColor.gray
        } else {
            image1.backgroundColor = UIColor.clear
            image2.backgroundColor = UIColor.clear
        }
    }
}

extension CompareViewController : UIDragInteractionDelegate {
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        let draglocation = session.location(in: view)

        if image1.frame.contains(draglocation),
            device1Index >= 0,
            let image = image1.image {
            let itemProvider = NSItemProvider(object: image)
            let dragItem = UIDragItem(itemProvider: itemProvider)
            dragItem.localObject = device1Index
            return [dragItem]
        } else if image2.frame.contains(draglocation),
            device2Index >= 0,
            let image = image2.image {
            let itemProvider = NSItemProvider(object: image)
            let dragItem = UIDragItem(itemProvider: itemProvider)
            dragItem.localObject = device2Index
            return [dragItem]
        }
        return []
    }

    func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
        let draglocation = session.location(in: view)

        return  image1.frame.contains(draglocation) ?
            UITargetedDragPreview(view: image1) :
            UITargetedDragPreview(view: image2)
    }
}
