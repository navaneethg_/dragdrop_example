//
//  DevicesViewController.swift
//  Drag-Drop
//
//  Created by navaneeth on 12/12/18.
//  Copyright © 2018 com.sprint. All rights reserved.
//

import UIKit

class DevicesViewController: ShopBaseVC {

    @IBOutlet weak var devicesTabItem: UITabBarItem!
    @IBOutlet weak var mainStackview: UIStackView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setAppreances()
    }

    func setAppreances() {
        addDragInteraction()
        var hStackView = getHStackView()
        var count = 3
        for view in deviceViews {
            if count < 1 {
                mainStackview.addArrangedSubview(hStackView)
                hStackView = getHStackView()
                count = 3
            }
            hStackView.addArrangedSubview(view)
            count -= 1
        }
        mainStackview.addArrangedSubview(getChoosePlanView())
    }

    func getHStackView() -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        return stackView
    }

    func getChoosePlanView() -> UIView {
        let choosePlanView = UIView()
        let button = UIButton()
        button.backgroundColor = UIColor.lightGray
        button.layer.cornerRadius  = 20
        button.layer.masksToBounds  = true
        button.setTitle("Choose Plan", for: .normal)
        choosePlanView.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.leftAnchor.constraint(equalTo: choosePlanView.leftAnchor, constant: 10).isActive = true
        choosePlanView.rightAnchor.constraint(equalTo: button.rightAnchor, constant: 10).isActive = true
        button.topAnchor.constraint(equalTo: choosePlanView.topAnchor, constant: 50).isActive = true
        button.heightAnchor.constraint(equalToConstant: 70).isActive = true
        button.addTarget(self, action: #selector(gotoChoosePlan), for: .touchUpInside)
        return choosePlanView
    }

    @objc func gotoChoosePlan(){
        if let vc = UIStoryboard(name: "Plans", bundle: nil).instantiateInitialViewController() {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    func addDragInteraction() {
        let dragInteraction = UIDragInteraction(delegate: self)
        view.addInteraction(dragInteraction)
        dragInteraction.isEnabled = true
    }
}

extension DevicesViewController: UIDragInteractionDelegate {
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        return getItems(session:session)
    }

    func getItems(session : UIDragSession, at point : CGPoint? = nil) -> [UIDragItem] {
        if let index = touchedViewIndex(at: point ?? session.location(in: view)) {
            let device = deviceViews[index]
            if let image  = device.deviceImageview.image {
                let itemProvider = NSItemProvider(object: image)
                let dragItem = UIDragItem(itemProvider: itemProvider)
                dragItem.previewProvider = { () -> UIDragPreview in
                    let previewView = device.getCustomLiftPreview(image: image)

                    let inflatedBounds = previewView.bounds.insetBy(dx: -90, dy: -10)
                    let parameters = UIDragPreviewParameters()
                    parameters.visiblePath = UIBezierPath(roundedRect: inflatedBounds, cornerRadius: 20)
                    return UIDragPreview(view: previewView, parameters: parameters)
                }
                dragItem.localObject = index
                return [ dragItem]
            }
        }
        return []
    }

    func touchedViewIndex(at point: CGPoint) -> Int? {
        if let hitTestView = view?.hitTest(point, with: nil) as? DeviceView,
            let index = deviceViews.index(of: hitTestView) {
            return index
        }
        return nil
    }

    func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
        let index = item.localObject as! Int
        let previewView = deviceViews[index]
        return UITargetedDragPreview(view: previewView)
    }

    func dragInteraction(_ interaction: UIDragInteraction, itemsForAddingTo session: UIDragSession, withTouchAt point: CGPoint) -> [UIDragItem] {
        return getItems(session: session, at : point)
    }

    func dragInteraction(_ interaction: UIDragInteraction, previewForCancelling item: UIDragItem,
                         withDefault defaultPreview: UITargetedDragPreview) -> UITargetedDragPreview? {


        if self.tabBarController?.tabBar.selectedItem == devicesTabItem {
            let index = item.localObject as! Int
            let previewView = deviceViews[index]
            return UITargetedDragPreview(view: previewView)
        } else {
            return defaultPreview
        }
    }
}
