//
//  DeviceView.swift
//  Drag-Drop
//
//  Created by navaneeth on 12/12/18.
//  Copyright © 2018 com.sprint. All rights reserved.
//

import UIKit

class DeviceView : UIView  {
    @IBOutlet weak var deviceImageview: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var name: UILabel!

    @IBOutlet weak var memory2: UIButton!
    @IBOutlet weak var memory1: UIButton!
    @IBOutlet weak var color2: UIButton!
    @IBOutlet weak var color1: UIButton!

//    func getCustomLiftPreview() -> DeviceView {
//        let view   = self
//        view.alpha = 0.6
//        return view
//    }

    func getCustomLiftPreview(image: UIImage) -> UIView {
        guard let customview =  Bundle.main.loadNibNamed("DeviceView", owner: nil, options: nil)?.last as? DeviceView else {
                    let customview = UIView(frame:  CGRect(x: 0, y: 0, width: 160, height: 150))
                    let imageview = UIImageView(image: image)
                    imageview.layer.cornerRadius = 20
                    imageview.layer.masksToBounds = true
                    customview.addSubview(imageview)
                return customview
        }
        customview.frame = self.frame
        customview.bounds = self.bounds
        customview.deviceImageview.image = image
        customview.name.text = name.text
        customview.color2.isHidden = true
        customview.memory2.isHidden = true
        customview.color1.titleLabel?.text = (color1.isEnabled == false) ? color1.titleLabel?.text : color2.titleLabel?.text
        customview.color1.backgroundColor = (color1.isEnabled == false) ? color1.backgroundColor : color2.backgroundColor

        customview.memory1.titleLabel?.text = (memory1.isEnabled == false) ? memory1.titleLabel?.text : memory2.titleLabel?.text
        return customview
    }

    @IBAction func color1Tap(_ sender: Any) {
        color1.isSelected = true
        color2.isSelected = false
        color1.isEnabled = false
        color2.isEnabled = true
    }

    @IBAction func color2Tap(_ sender: Any) {
        color2.isSelected = true
        color1.isSelected = false
        color2.isEnabled = false
        color1.isEnabled = true
    }

    @IBAction func memeory1Tap(_ sender: Any) {
        memory1.isSelected = true
        memory2.isSelected = false
        memory1.isEnabled = false
        memory2.isEnabled = true
    }

    @IBAction func memeory2Tap(_ sender: Any) {
        memory2.isSelected = true
        memory1.isSelected = false
        memory2.isEnabled = false
        memory1.isEnabled = true
    }
}

class DeviceEntity {
    let id : Int?
    let image : UIImage? = UIImage(named: "iphone_single")
    let price : String?
    let name : String?
    let color : UIColor?
    let size: String?

    init(id : Int, price : String, name : String, color : UIColor,size : String) {
        self.id = id
        self.price = price
        self.name = name
        self.color = color
        self.size = size
    }
}


class Plan {
    let planName: String
    let cost: String
    let hotspot:String
    let extras : Bool

    init(name: String, cost:String, hotspot: String , extras: Bool){
        self.planName = name
        self.cost = cost
        self.hotspot = hotspot
        self.extras = extras
    }
}
